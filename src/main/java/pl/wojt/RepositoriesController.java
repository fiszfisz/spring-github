package pl.wojt;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RepositoriesController {

    @RequestMapping("/repositories")
    public String repositories() {
        return "Repository";
    }

}
